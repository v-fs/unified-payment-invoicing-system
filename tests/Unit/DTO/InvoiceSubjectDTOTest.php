<?php

declare(strict_types=1);

namespace Tests\Unit\DTO;

use Hejna\UnifiedPaymentInvoicingSystem\DTO\InvoiceSubjectDTO;
use PHPUnit\Framework\TestCase;

/**
 * Testovací třída pro InvoiceSubjectDTO.
 * Ověřuje, že všechny vlastnosti subjektu faktury jsou správně nastaveny a mohou být korektně získány.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
class InvoiceSubjectDTOTest extends TestCase
{
    /**
     * Testuje přiřazení a získání vlastností objektu InvoiceSubjectDTO.
     * Ověřuje, že každá vlastnost nastavená prostřednictvím konstruktoru je správně uložena
     * a může být korektně získána.
     */
    public function testPropertyAssignment(): void
    {
        $id = 1;
        $customId = 'customId';
        $type = 'type';
        $name = 'name';
        $street = 'street';
        $street2 = 'street2';
        $city = 'city';
        $zip = 'zip';
        $country = 'country';
        $registrationNo = 'registrationNo';
        $vatNo = 'vatNo';
        $localVatNo = 'localVatNo';
        $bankAccount = 'bankAccount';
        $iban = 'iban';
        $variableSymbol = 'variableSymbol';
        $fullName = 'fullName';
        $email = 'email@example.com';
        $emailCopy = 'emailCopy@example.com';
        $phone = '123456789';
        $privateNote = 'privateNote';
        $url = 'http://example.com';
        $createdAt = '2023-01-01';
        $updatedAt = '2023-01-02';

        $dto = new InvoiceSubjectDTO(
            id: $id,
            customId: $customId,
            type: $type,
            name: $name,
            street: $street,
            street2: $street2,
            city: $city,
            zip: $zip,
            country: $country,
            registrationNo: $registrationNo,
            vatNo: $vatNo,
            localVatNo: $localVatNo,
            bankAccount: $bankAccount,
            iban: $iban,
            variableSymbol: $variableSymbol,
            enabledReminders: true,
            fullName: $fullName,
            email: $email,
            emailCopy: $emailCopy,
            phone: $phone,
            privateNote: $privateNote,
            url: $url,
            createdAt: $createdAt,
            updatedAt: $updatedAt
        );

        $this->assertEquals($id, $dto->id);
        $this->assertEquals($customId, $dto->customId);
        $this->assertEquals($updatedAt, $dto->updatedAt);
        $this->assertEquals($type, $dto->type);
        $this->assertEquals($name, $dto->name);
        $this->assertEquals($street, $dto->street);
        $this->assertEquals($street2, $dto->street2);
        $this->assertEquals($city, $dto->city);
        $this->assertEquals($zip, $dto->zip);
        $this->assertEquals($country, $dto->country);
        $this->assertEquals($registrationNo, $dto->registrationNo);
        $this->assertEquals($vatNo, $dto->vatNo);
        $this->assertEquals($localVatNo, $dto->localVatNo);
        $this->assertEquals($bankAccount, $dto->bankAccount);
        $this->assertEquals($iban, $dto->iban);
        $this->assertEquals($variableSymbol, $dto->variableSymbol);
        $this->assertTrue($dto->enabledReminders);
        $this->assertEquals($fullName, $dto->fullName);
        $this->assertEquals($email, $dto->email);
        $this->assertEquals($emailCopy, $dto->emailCopy);
        $this->assertEquals($phone, $dto->phone);
        $this->assertEquals($privateNote, $dto->privateNote);
        $this->assertEquals($url, $dto->url);
        $this->assertEquals($createdAt, $dto->createdAt);
    }
}
