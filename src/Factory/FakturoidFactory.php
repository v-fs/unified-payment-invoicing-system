<?php

declare(strict_types=1);

namespace Hejna\UnifiedPaymentInvoicingSystem\Factory;

use Fakturoid\FakturoidManager;
use GuzzleHttp\Client as GuzzleClient;

class FakturoidFactory
{
    /**
     * Creates and returns a configured FakturoidManager instance.
     */
    public static function createManager(): FakturoidManager
    {
        // Instantiate a Laravel-friendly PSR-18 HTTP client using Guzzle.
        $httpClient = new GuzzleClient();

        // Retrieve configuration values for the v3 authentication.
        $clientId     = config('invoice.client_init.clientId');
        $clientSecret = config('invoice.client_init.clientSecret');
        $userAgent    = config('invoice.client_init.invoiceUserAgent');
        $accountSlug  = config('invoice.client_init.accountSlug', null);

        // Create the FakturoidManager using client credentials flow.
        $manager = new FakturoidManager(
            $httpClient,
            $clientId,
            $clientSecret,
            $userAgent
        );

        if ($accountSlug !== null) {
            $manager->setAccountSlug($accountSlug);
        }

        $manager->authClientCredentials();

        return $manager;
    }
}
