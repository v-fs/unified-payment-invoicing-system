<?php

declare(strict_types=1);

namespace Hejna\UnifiedPaymentInvoicingSystem\Interface;

use Hejna\UnifiedPaymentInvoicingSystem\DTO\InvoiceSubjectDTO;
use Hejna\UnifiedPaymentInvoicingSystem\DTO\UserDTO;
use Hejna\UnifiedPaymentInvoicingSystem\Enum\InvoiceActionEnum;
use Illuminate\Support\Carbon;

/**
 * Rozhraní InvoiceServiceInterface definuje základní funkcionality pro manipulaci s fakturami v systému.
 * Obsahuje metody pro vytváření a hledání subjektu faktury, vytváření a aktualizaci faktur a získávání binárních dat faktur.
 * Tato rozhraní je implementována třídou, která slouží k integraci s konkrétním fakturačním systémem.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
interface InvoiceServiceInterface
{
    /**
     * Vytvoří nebo najde subjekt faktury na základě uživatelských údajů.
     *
     * @param UserDTO $user Objekt obsahující údaje uživatele.
     * @return InvoiceSubjectDTO Vytvořený nebo nalezený subjekt faktury.
     */
    public function createOrFindInvoiceSubject(UserDTO $user): InvoiceSubjectDTO;

    /**
     * Vytvoří novou fakturu na základě poskytnutých produktů, identifikátoru subjektu, události a data.
     *
     * @param array $productDTOs Pole produktů, které mají být fakturovány.
     * @param int $subjectId Identifikátor subjektu faktury.
     * @param InvoiceActionEnum $event Událost, která spouští vytvoření faktury.
     * @param Carbon $date Datum vytvoření faktury.
     * @return int Identifikátor vytvořené faktury.
     */
    public function createInvoice(
        array $productDTOs,
        int $subjectId,
        InvoiceActionEnum $event,
        ?Carbon $date = null
    ): int;

    /**
     * Aktualizuje existující fakturu na základě jejího identifikátoru a data.
     *
     * @param int $invoiceId Identifikátor faktury, která má být aktualizována.
     * @param string $date Nové datum faktury.
     * @return int Identifikátor aktualizované faktury.
     */
    public function updateInvoice(int $invoiceId, string $date): int;

    /**
     * Získá binární data faktury na základě jejího čísla.
     *
     * @param string $invoiceNumber Číslo faktury.
     * @return string Binární data faktury.
     */
    public function getBinaryInvoice(string $invoiceNumber): string;
}
