<?php

declare(strict_types=1);

use GoPay\Definition\Language;
use GoPay\Definition\Payment\BankSwiftCode;
use GoPay\Definition\Payment\Currency;
use GoPay\Definition\Payment\PaymentInstrument;
use GoPay\Definition\TokenScope;

return [
    'client_init' => [
        'goid' => env('PAYMENT_ID'),
        'clientId' => env('PAYMENT_CLIENT_ID'),
        'clientSecret' => env('PAYMENT_CLIENT_SECRET'),
        'gatewayUrl' => env('PAYMENT_GATEWAY_URL', config('https://gw.sandbox.gopay.com')),
        'scope' =>  TokenScope::ALL,
        'language' => Language::CZECH,
        'timeout' => 30,
    ],
    'payment_settings' => [
        'default_payment_instrument' => PaymentInstrument::PAYMENT_CARD,
        'allowed_payment_instruments' => [PaymentInstrument::PAYMENT_CARD],
        'default_swift' => BankSwiftCode::KOMERCNI_BANKA,
        'allowed_swifts' => [
            BankSwiftCode::FIO_BANKA,
            BankSwiftCode::MBANK,
            BankSwiftCode::KOMERCNI_BANKA,
            BankSwiftCode::CSOB,
            BankSwiftCode::RAIFFEISENBANK,
        ],
        'currency' => Currency::CZECH_CROWNS,
        'order_description' => 'Objednávka z webu',
        'lang' => Language::CZECH,
        'amount_multiplier' => 100,
        'callback_urls' => [
            'return_url' => 'orderStatus',
            'notification_url' => 'orderNotification',
        ],
    ],
];
