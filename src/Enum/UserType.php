<?php

declare(strict_types=1);

namespace Hejna\UnifiedPaymentInvoicingSystem\Enum;

/**
 * Výčtový typ UserType definuje různé typy uživatelů v systému.
 * Tyto typy zahrnují běžného uživatele, administrátora, administrátora s omezeným přístupem a návrháře.
 * Každý typ uživatele je reprezentován jedinečnou hodnotou v tomto výčtu.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
enum UserType: string
{
    case USER = 'user';
    case ADMIN = 'admin';
    case ADMIN_RESTRICTED = 'adminRestricted';
    case DRAFT = 'draft';
}
