# UnifiedPaymentInvoicingSystem

## Obecné informace

Tento balíček slouží k integraci platebních a fakturačních operací do webových aplikací využívajících framework Laravel.  
Byl navržen s důrazem na snadnou integraci s platebními systémy jako [GoPay](https://doc.gopay.com/) a fakturačními systémy jako [Fakturoid](https://github.com/fakturoid/fakturoid-php). 
Package je součástí mé bakalářské práce na téma "Integrace platebního systému do webových aplikací".

**Autor:** Daniel Hejna  
**Rok vytvoření:** 2024  
**Fakulta:** Ekonomických studií na Vysoké škole finanční a správní  
**Studijní obor:** Aplikovaná informatika  
**Název BC práce:** Integrace platebního systému do webových aplikací

## Další informace

- Balíček si klade za cíl dosáhnout 100% code coverage(neboli pokrýt každou řádku kódu testy) s PHPUnit.
- V projektu je použit [PHPStan](https://phpstan.org/) level 8 pro statickou analýzu kódu.
- Pro zajištění konzistentního kódování a dodržování nejlepších praktik využíváme [Easy Coding Standard](https://github.com/easy-coding-standard/easy-coding-standard) a [Rector](https://github.com/rectorphp/rector).

## Publikace nových verzí

Nové verze se publikují vytvořením tagu nad hlavní větví `main`. 
Příspěvky do projektu jsou vítány formou Pull Requestů.

## Instalace package

Package je určen pro aplikace Laravel. Pro jeho instalaci použijte příkaz:

`composer require hejna/unified-payment-invoicing-system
`

**Po instalaci publikujte konfigurační soubory:**

`
php artisan vendor:publish --provider="Hejna\UnifiedPaymentInvoicingSystem\PaymentInvoicingSystemProvider"
`

**Minimální požadavky**

Pro použití je nutné mít testovací údaje od GoPay a Fakturoid. 

**Vyplňte následující environmentální proměnné v .env souboru:**

**Fakturoid:**
```
INVOICE_USERNAME=vaše_fakturoid_uživatelské_jméno
INVOICE_EMAIL=vaše_fakturoid_emailová_adresa
INVOICE_API_KEY=vaše_fakturoid_api_klíč
INVOICE_USER_AGENT=identifikátor_vaší_aplikace_pro_fakturoid_api
```

**GoPay:**
```
PAYMENT_ID=vaše_gopay_obchodník_id
PAYMENT_CLIENT_ID=vaše_gopay_client_id
PAYMENT_CLIENT_SECRET=vaše_gopay_client_secret
PAYMENT_GATEWAY_URL=https://gw.sandbox.gopay.com
```

## Použití

Balíček poskytuje rozhraní pro práci s platbami (PaymentServiceInterface) a fakturací (InvoiceServiceInterface).

**Příklady základního použití:**

**Vytvoření platby:**
`$response = $paymentService->createPayment($orderNumber, $user, $amount, $products);
`

**Získání stavu platby:**
`$paymentStatus = $paymentService->getPaymentStatus($id);
`

**Refundace platby:**
`$response = $paymentService->refundPayment($id, $amount);
`

**Vytvoření faktury:**
`$invoiceId = $invoiceService->createInvoice($products, $subjectId, $event, $date);
`

**Získání PDF faktury:**
`$pdfContent = $invoiceService->getBinaryInvoice($invoiceNumber);
`


Pro detailní popis metod a jejich parametrů naleznete v dokumentaci kódu.

Tento dokument poskytuje základní přehled a návod k použití package `Unified Payment and Invoicing System`. <br/>
Pro hlubší pochopení a pokročilé funkcionality doporučuji prozkoumat kód a případně se podívat do dokumentace jednotlivých služeb (GoPay, Fakturoid).


