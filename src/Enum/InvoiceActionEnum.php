<?php

declare(strict_types=1);

namespace Hejna\UnifiedPaymentInvoicingSystem\Enum;

/**
 * Výčtový typ InvoiceActionEnum definuje možné akce, které lze provést s fakturou.
 * Tento výčtový typ obsahuje seznam akcí, které mohou být aplikovány na faktury v systému.
 * Jednotlivé hodnoty reprezentují různé akce, jako je označení jako odeslaná, doručení, platba, atd.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
enum InvoiceActionEnum: string
{
    case MARK_AS_SENT = 'mark_as_sent';
    case DELIVER = 'deliver';
    case PAY = 'pay';
    case PAY_PROFORMA = 'pay_proforma';
    case PAY_PARTIAL_PROFORMA = 'pay_partial_proforma';
    case REMOVE_PAYMENT = 'remove_payment';
    case DELIVER_REMINDER = 'deliver_reminder';
    case CANCEL = 'cancel';
    case UNDO_CANCEL = 'undo_cancel';
    case LOCK = 'lock';
    case UNLOCK = 'unlock';
    case MARK_AS_UNCOLLECTIBLE = 'mark_as_uncollectible';
    case UNDO_UNCOLLECTIBLE = 'undo_uncollectible';
}
