<?php

declare(strict_types=1);

namespace Hejna\UnifiedPaymentInvoicingSystem\Service;

use Exception;
use Fakturoid\Exception\InvalidResponseException;
use Hejna\UnifiedPaymentInvoicingSystem\DTO\InvoiceSubjectDTO;
use Hejna\UnifiedPaymentInvoicingSystem\DTO\UserDTO;
use Hejna\UnifiedPaymentInvoicingSystem\Enum\InvoiceActionEnum;
use Hejna\UnifiedPaymentInvoicingSystem\Exception\NotExistInvoiceException;
use Hejna\UnifiedPaymentInvoicingSystem\Factory\FakturoidFactory;
use Hejna\UnifiedPaymentInvoicingSystem\Interface\InvoiceServiceInterface;
use Hejna\UnifiedPaymentInvoicingSystem\Validation\FakturoidValidator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class FakturoidService implements InvoiceServiceInterface
{
    public function __construct(
        private readonly FakturoidValidator $invoiceValidator,
    )
    {
        $this->invoiceValidator->validateConfiguration();
    }

    /**
     * Creates a new invoice subject or finds an existing one based on the user’s email.
     */
    public function createOrFindInvoiceSubject(UserDTO $user): InvoiceSubjectDTO
    {
        $manager = $this->clientInit();
        $subjectResponse = $manager->getSubjectsProvider()->list(['custom_id' => $user->email]);
        $subjects = $subjectResponse->getBody();

        if (!empty($subjects)) {
            return $this->mapToInvoiceSubjectDTO((array)$subjects[0]);
        }

        return $this->createSubjectAndMapToDTO($user);
    }

    /**
     * Creates a new invoice subject in Fakturoid and maps it to InvoiceSubjectDTO.
     */
    public function createSubjectAndMapToDTO(UserDTO $user): InvoiceSubjectDTO
    {
        $manager = $this->clientInit();

        $payload = [
            'name' => $user->firstName . ' ' . $user->lastName,
            'email' => $user->email,
            'custom_id' => $user->email,
            'type' => 'customer',
            'street' => trim($user->deliveryInformations->street . ' ' . $user->deliveryInformations->streetNumber),
            'zip' => $user->deliveryInformations->postalCode,
            'city' => $user->deliveryInformations->city,
            'full_name' => $user->firstName . ' ' . $user->lastName,
            'phone' => $user->phoneNumber,
        ];

        Log::info('Subject creation request:', $payload);

        try {
            $response = $manager->getSubjectsProvider()->create($payload);
            $newSubject = $response->getBody();
            return $this->mapToInvoiceSubjectDTO((array)$newSubject);
        } catch (\Fakturoid\Exception\ClientErrorException $e) {
            Log::error('Fakturoid API Error: ' . $e->getResponse()->getBody()->getContents());
            throw new Exception("Fakturoid error: " . $e->getMessage());
        }
    }

    /**
     * Creates an invoice from the given product DTOs and subject.
     * Fires an invoice action afterwards – if the action corresponds to payment,
     * the invoice is marked as paid.
     */
    public function createInvoice(
        array             $productDTOs,
        int               $subjectId,
        InvoiceActionEnum $event,
        ?Carbon           $date = null
    ): int
    {
        $this->invoiceValidator->validateProducts($productDTOs);

        $date = $date ?? Carbon::now();
        $formattedDate = $date->format('Y-m-d');

        // ✅ Convert product DTOs to correct format.
        $products = array_map(static fn($productDTO) => $productDTO->toArray(), $productDTOs);

        $manager = $this->clientInit();

        $payload = [
            'subject_id' => $subjectId,
            'lines' => $products,
            'issued_on' => $formattedDate, // ✅ Ensure correct date format
            'due_on' => $formattedDate,
            'variable_symbol' => (string)rand(100000, 999999), // ✅ Add variable symbol
            'payment_method' => 'bank', // ✅ Example payment method
        ];

        Log::info('Invoice creation request:', $payload);

        try {
            $response = $manager->getInvoicesProvider()->create($payload);
            $invoice = $response->getBody();
        } catch (\Fakturoid\Exception\ClientErrorException $e) {

            $errorResponse = json_decode($e->getResponse()->getBody()->getContents(), true);

            // ✅ Log the error instead of `dd()`
            Log::error('Fakturoid API Error:', $errorResponse);

            throw new Exception("Fakturoid error: " . $e->getMessage());
        }

        // ✅ If action is pay, mark as paid
        if (in_array($event->value, ['pay', 'pay_proforma', 'pay_partial_proforma'], true)) {
            $manager->getInvoicesProvider()->createPayment($invoice->id, [
                'paid_on' => $formattedDate,
                'send_thank_you_email' => true,
            ]);
        } else {
            $manager->getInvoicesProvider()->fireAction($invoice->id, $event->value);
        }

        return $invoice->id;

    }

    /**
     * Retrieves the binary PDF of an invoice by its number.
     * @throws InvalidResponseException
     */
    public function getBinaryInvoice(string $invoiceNumber): string
    {
        $manager = $this->clientInit();

        try {
            $response = $manager->getInvoicesProvider()->getPdf((int)$invoiceNumber);
            if ($response->getStatusCode() !== 200) {
                throw new NotExistInvoiceException(sprintf('Invoice with number %s does not exist.', $invoiceNumber));
            }
        } catch (Exception $exception) {
            throw new NotExistInvoiceException('Failed to retrieve invoice: ' . $exception->getMessage(), 0, $exception);
        }

        return $response->getBody();
    }

    /**
     * Initializes and returns the Fakturoid manager.
     */
    public function clientInit(): \Fakturoid\FakturoidManager
    {
        return FakturoidFactory::createManager();
    }

    /**
     * Updates the invoice by marking it as paid without sending an email.
     */
    public function updateInvoice(int $invoiceId, string $date): int
    {
        $manager = $this->clientInit();
        $manager->getInvoicesProvider()->createPayment($invoiceId, [
            'paid_on' => $date,
            'send_thank_you_email' => false,
        ]);

        return 1;
    }

    /**
     * Maps an array of Fakturoid subject data to InvoiceSubjectDTO.
     */
    private function mapToInvoiceSubjectDTO(array $data): InvoiceSubjectDTO
    {
        return new InvoiceSubjectDTO(
            id: $data['id'],
            customId: $data['custom_id'] ?? null,
            type: $data['type'],
            name: $data['name'],
            street: $data['street'] ?? null,
            street2: $data['street2'] ?? null,
            city: $data['city'],
            zip: $data['zip'],
            country: $data['country'],
            registrationNo: $data['registration_no'] ?? null,
            vatNo: $data['vat_no'] ?? null,
            localVatNo: $data['local_vat_no'] ?? null,
            bankAccount: $data['bank_account'] ?? null,
            iban: $data['iban'] ?? null,
            variableSymbol: $data['variable_symbol'] ?? null,
            enabledReminders: $data['enabled_reminders'] ?? false,
            fullName: $data['full_name'],
            email: $data['email'],
            emailCopy: $data['email_copy'] ?? null,
            phone: $data['phone'] ?? null,
            privateNote: $data['private_note'] ?? null,
            url: $data['url'] ?? '',
            createdAt: $data['created_at'] ?? '',
            updatedAt: $data['updated_at'] ?? ''
        );
    }
}
