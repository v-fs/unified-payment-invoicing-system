<?php

declare(strict_types=1);

namespace Tests\Unit\DTO;

use Hejna\UnifiedPaymentInvoicingSystem\DTO\DeliveryInformationDTO;
use PHPUnit\Framework\TestCase;

/**
 * Testovací třída pro DeliveryInformationDTO.
 * Ověřuje, že data předaná do DTO jsou správně nastavena a mohou být korektně získána
 * pomocí metody toArray.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
class DeliveryInformationDTOTest extends TestCase
{
    /**
     * Testuje, že metoda toArray vrací očekávané pole hodnot.
     * Ověřuje, že všechny vlastnosti objektu jsou správně převedeny na pole.
     */
    public function testToArray(): void
    {
        $country = 'Country';
        $street = 'Street';
        $streetNumber = '123';
        $postalCode = '00000';
        $city = 'City';
        $countryCode = 'CC';

        $dto = new DeliveryInformationDTO(
            country: $country,
            street: $street,
            streetNumber: $streetNumber,
            postalCode: $postalCode,
            city: $city,
            countryCode: $countryCode
        );

        $expected = [
            'country' => $country,
            'street' => $street,
            'streetNumber' => $streetNumber,
            'postalCode' => $postalCode,
            'city' => $city,
            'countryCode' => $countryCode,
        ];

        $this->assertEquals($expected, $dto->toArray(), 'The toArray method did not return the expected array.');
    }

    /**
     * Testuje, že přiřazení hodnot prostřednictvím konstruktoru správně nastaví vlastnosti objektu.
     * Současně ověřuje, že metoda toArray vrací očekávané pole hodnot.
     */
    public function testPropertyAssignmentAndToArray(): void
    {
        $country = 'Country';
        $street = 'Street';
        $streetNumber = '123';
        $postalCode = '00000';
        $city = 'City';
        $countryCode = 'CC';

        $dto = new DeliveryInformationDTO(
            country: $country,
            street: $street,
            streetNumber: $streetNumber,
            postalCode: $postalCode,
            city: $city,
            countryCode: $countryCode
        );

        $this->assertEquals($country, $dto->country, 'The country property is not correctly assigned.');
        $this->assertEquals($street, $dto->street, 'The street property is not correctly assigned.');
        $this->assertEquals($streetNumber, $dto->streetNumber, 'The streetNumber property is not correctly assigned.');
        $this->assertEquals($postalCode, $dto->postalCode, 'The postalCode property is not correctly assigned.');
        $this->assertEquals($city, $dto->city, 'The city property is not correctly assigned.');
        $this->assertEquals($countryCode, $dto->countryCode, 'The countryCode property is not correctly assigned.');

        $expected = [
            'country' => $country,
            'street' => $street,
            'streetNumber' => $streetNumber,
            'postalCode' => $postalCode,
            'city' => $city,
            'countryCode' => $countryCode,
        ];

        $this->assertEquals($expected, $dto->toArray(), 'The toArray method did not return the expected array.');
    }
}
