<?php

declare(strict_types=1);

namespace Hejna\UnifiedPaymentInvoicingSystem;

use Hejna\UnifiedPaymentInvoicingSystem\Interface\InvoiceServiceInterface;
use Hejna\UnifiedPaymentInvoicingSystem\Interface\PaymentServiceInterface;
use Hejna\UnifiedPaymentInvoicingSystem\Service\FakturoidService;
use Hejna\UnifiedPaymentInvoicingSystem\Service\GoPayService;
use Hejna\UnifiedPaymentInvoicingSystem\Validation\GoPayPaymentValidator;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\ServiceProvider;

/**
 * Třída PaymentProvider slouží k registraci služeb týkajících se platebního a fakturačního systému v aplikaci.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
class PaymentInvoicingSystemProvider extends ServiceProvider
{
    /**
     * Registruje služby potřebné pro platební a fakturační systém v Laravel aplikaci.
     */
    public function register(): void
    {
        $this->bindServices();
        $this->mergePackageConfig();
    }

    /**
     * Inicializuje služby potřebné pro aplikaci po jejím spuštění.
     */
    public function boot(): void
    {
        $this->publishPackageConfig();
    }

    /**
     * Vytváří vazby pro služby týkající se plateb a fakturace v kontejneru služeb.
     */
    protected function bindServices(): void
    {
        $this->app->bind(GoPayPaymentValidator::class, static fn () => new GoPayPaymentValidator());

        $this->app->singleton(
            GoPayService::class,
            static fn ($app) => new GoPayService($app->make(UrlGenerator::class), $app->make(
                GoPayPaymentValidator::class
            ))
        );

        $this->app->bind(PaymentServiceInterface::class, GoPayService::class);
        $this->app->bind(InvoiceServiceInterface::class, FakturoidService::class);
    }

    /**
     * Sloučí konfigurační soubory balíčku s konfigurací aplikace.
     */
    protected function mergePackageConfig(): void
    {
        $this->mergeConfigFrom(__DIR__.'/../config/payment.php', 'payment');
        $this->mergeConfigFrom(__DIR__.'/../config/invoice.php', 'invoice');
    }

    /**
     * Publikuje konfigurační soubory balíčku, aby byly dostupné v aplikaci.
     */
    protected function publishPackageConfig(): void
    {
        $this->publishes([
            __DIR__.'/../config/payment.php' => config_path('payment.php'),
            __DIR__.'/../config/invoice.php' => config_path('invoice.php'),
        ], 'config');
    }
}
