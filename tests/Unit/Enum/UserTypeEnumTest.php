<?php

declare(strict_types=1);

namespace Tests\Unit\Enum;

use Hejna\UnifiedPaymentInvoicingSystem\Enum\UserType;
use PHPUnit\Framework\TestCase;

/**
 * Testovací třída pro UserType enum.
 * Ověřuje správnost a funkčnost definovaných hodnot enumu UserType a jejich použití.
 * Zaměřuje se na ověření správných řetězcových hodnot pro jednotlivé případy enumu a
 * také na přítomnost všech očekávaných případů enumu.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
class UserTypeEnumTest extends TestCase
{
    /**
     * Testuje, že hodnoty enumu UserType jsou správně přiřazeny.
     * Ověřuje očekávané stringové hodnoty pro jednotlivé případy enumu.
     */
    public function testUserTypeEnumValues(): void
    {
        $this->assertEquals('user', UserType::USER->value);
        $this->assertEquals('admin', UserType::ADMIN->value);
        $this->assertEquals('adminRestricted', UserType::ADMIN_RESTRICTED->value);
        $this->assertEquals('draft', UserType::DRAFT->value);
    }

    /**
     * Testuje, že všechny případy enumu UserType jsou správně definovány a přítomné.
     * Ověřuje, že pole vracené metodou cases() obsahuje všechny očekávané případy enumu
     * a že jejich počet odpovídá očekávanému. Porovnává také, zda hodnoty enumu
     * odpovídají očekávaným hodnotám definovaným v testu.
     */
    public function testEnumCases(): void
    {
        $cases = UserType::cases();
        $expectedValues = [
            'user' => UserType::USER,
            'admin' => UserType::ADMIN,
            'adminRestricted' => UserType::ADMIN_RESTRICTED,
            'draft' => UserType::DRAFT,
        ];

        foreach ($expectedValues as $value => $expectedCase) {
            $this->assertTrue(
                in_array($expectedCase, $cases, true),
                sprintf("The case for '%s' is correctly defined.", $value)
            );
        }

        $this->assertCount(count($expectedValues), $cases, 'All expected enum cases are present.');
    }
}
