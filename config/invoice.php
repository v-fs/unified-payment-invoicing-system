<?php

declare(strict_types=1);

return [
    'client_init' => [
        'clientId'       => env('INVOICE_CLIENT_ID'),
        'clientSecret'   => env('INVOICE_CLIENT_SECRET'),
        'invoiceUserAgent' => env('INVOICE_USER_AGENT'),
        'accountSlug'    => env('INVOICE_ACCOUNT_SLUG'),
    ],
];
