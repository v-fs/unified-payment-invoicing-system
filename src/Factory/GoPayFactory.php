<?php

declare(strict_types=1);

namespace Hejna\UnifiedPaymentInvoicingSystem\Factory;

use GoPay\Definition\Language;
use GoPay\Definition\TokenScope;
use GoPay\Payments;

/**
 * Factory class pro vytvoření a konfiguraci instance GoPay Payments.
 * Tato třída poskytuje centralizovaný způsob vytvoření a konfigurace instance GoPay Payments,
 * která je používána pro komunikaci s GoPay platební bránou. Konfigurace je načtena
 * z aplikace, což umožňuje snadnou správu a úpravu nastavení.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
class GoPayFactory
{
    /**
     * Vytvoří a vrátí nakonfigurovanou instanci GoPay Payments.
     *
     * Tato metoda načítá konfigurační hodnoty z aplikace a používá je
     * pro inicializaci a konfiguraci instance GoPay Payments. Je navržena
     * tak, aby bylo možné snadno spravovat a přizpůsobovat konfiguraci
     * komunikace s GoPay platební bránou.
     *
     * @return Payments Nakonfigurovaná instance GoPay Payments.
     */
    public static function createPayments(): Payments
    {
        return \GoPay\payments([
            'goid' => config('payment.client_init.goid'),
            'clientId' => config('payment.client_init.clientId'),
            'clientSecret' => config('payment.client_init.clientSecret'),
            'gatewayUrl' => config('payment.client_init.gatewayUrl'),
            'scope' => TokenScope::ALL,
            'language' => Language::CZECH,
            'timeout' => 30,
        ]);
    }
}
