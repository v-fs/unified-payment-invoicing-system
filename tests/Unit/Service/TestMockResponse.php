<?php

declare(strict_types=1);

namespace Tests\Unit\Service;

use stdClass;

/**
 * Třída TestMockResponse slouží jako zjednodušený mock objekt pro HTTP odpovědi.
 * Umožňuje definovat statusový kód a tělo odpovědi, které mohou být použity při testování
 * služeb volajících externí API nebo jakékoliv funkcionality závislé na HTTP odpovědích.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
class TestMockResponse
{
    /**
     * Konstruktor pro vytvoření instance TestMockResponse.
     *
     * @param int $statusCode Statusový kód HTTP odpovědi. Výchozí hodnota je 200.
     * @param StdClass $body Tělo HTTP odpovědi jako řetězec. Výchozí hodnota je prázdný řetězec.
     */
    public function __construct(
        public int $statusCode,
        public stdClass $body
    ) {
    }

    /**
     * Získá statusový kód HTTP odpovědi.
     *
     * @return int Statusový kód odpovědi.
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * Získá tělo HTTP odpovědi.
     */
    public function getBody()
    {
        return json_encode($this->body);
    }
}
