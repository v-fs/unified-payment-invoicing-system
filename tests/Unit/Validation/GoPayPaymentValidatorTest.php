<?php

declare(strict_types=1);

namespace Tests\Unit\Validation;

use GoPay\Definition\Language;
use GoPay\Definition\TokenScope;
use Hejna\UnifiedPaymentInvoicingSystem\DTO\DeliveryInformationDTO;
use Hejna\UnifiedPaymentInvoicingSystem\DTO\ProductPaymentFormatDTO;
use Hejna\UnifiedPaymentInvoicingSystem\DTO\UserDTO;
use Hejna\UnifiedPaymentInvoicingSystem\PaymentInvoicingSystemProvider;
use Hejna\UnifiedPaymentInvoicingSystem\Validation\GoPayPaymentValidator;
use Orchestra\Testbench\TestCase;

/**
 * Tato třída obsahuje testy pro ověření validace platebních informací v systému GoPay.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
class GoPayPaymentValidatorTest extends TestCase
{
    private GoPayPaymentValidator $validator;

    /**
     * Nastavuje prostředí pro každý test před jeho spuštěním.
     * Inicializuje validátor GoPayPayment pro použití v testech, což zahrnuje
     * přípravu potřebných objektů a konfigurací před každým testem.
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->validator = new GoPayPaymentValidator();
    }

    /**
     * Testuje vyhození výjimky pro neplatný e-mail.
     */
    public function testValidateEmailThrowsExceptionForInvalidEmail(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $user = new UserDTO(
            'Daniel',
            'Hejna',
            '',
            '774416609',
            new DeliveryInformationDTO('Česká Republika', 'Šeříková', '5.', '35002', 'Cheb', 'CZE')
        );
        $this->validator->validateEmail($user);
    }

    /**
     * Testuje úspěšnou validaci pro platný e-mail.
     */
    public function testValidateEmailSucceedsForValidEmail(): void
    {
        $user = new UserDTO(
            'Daniel',
            'Hejna',
            '39140@vsfs.cz',
            '774416609',
            new DeliveryInformationDTO('Česká Republika', 'Šeříková', '5.', '35002', 'Cheb', 'CZE')
        );
        $this->validator->validateEmail($user);

        $this->assertTrue(true);
    }

    /**
     * Testuje vyhození výjimky pro neplatné číslo objednávky.
     */
    public function testValidateOrderNumberThrowsExceptionForInvalidNumber(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->validator->validateOrderNumber(0);
    }

    /**
     * Testuje úspěšnou validaci pro platné číslo objednávky.
     */
    public function testValidateOrderNumberSucceedsForValidNumber(): void
    {
        $this->validator->validateOrderNumber(1);
        $this->assertTrue(true);
    }

    /**
     * Testuje vyhození výjimky pro neplatnou částku.
     */
    public function testValidateAmountThrowsExceptionForInvalidAmount(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->validator->validateAmount(0);
    }

    /**
     * Testuje úspěšnou validaci pro platnou částku.
     */
    public function testValidateAmountSucceedsForValidAmount(): void
    {
        $this->validator->validateAmount(100);
        $this->assertTrue(true);
    }

    /**
     * Testuje vyhození výjimky pro prázdný seznam produktů.
     */
    public function testValidateProductsThrowsExceptionForEmptyArray(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->validator->validateProducts([]);
    }

    /**
     * Testuje vyhození výjimky pro seznam obsahující neplatné produkty.
     */
    public function testValidateProductsThrowsExceptionForInvalidProducts(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->validator->validateProducts(['not-a-product']);
    }

    /**
     * Testuje úspěšnou validaci pro seznam platných produktů.
     */
    public function testValidateProductsSucceedsForValidProducts(): void
    {
        $product = new ProductPaymentFormatDTO('Example Product', 99.99, 1);
        $this->validator->validateProducts([$product]);
        $this->assertTrue(true);
    }

    /**
     * Testuje vyhození výjimky, pokud chybí konfigurace pro 'goid'.
     */
    public function testValidateConfigurationThrowsExceptionForMissingConfig(): void
    {
        $this->app['config']->set('payment.client_init.goid', null);

        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage("The configuration value for 'goid' must be provided in the gopay config.");

        $this->validator->validateConfiguration();
    }

    /**
     * Nastavuje testovací prostředí pro testovanou aplikaci.
     * Konfiguruje klienta GoPay s testovacími hodnotami, které umožňují simulaci
     * platebních operací bez nutnosti reálného připojení k platební bráně.
     */
    protected function getEnvironmentSetUp($app): void
    {
        $app['config']->set('payment.client_init', [
            'goid' => '123456789',
            'clientId' => 'test_client_id',
            'clientSecret' => 'test_client_secret',
            'gatewayUrl' => 'https://test-gateway.gopay.com',
            'scope' => TokenScope::ALL,
            'language' => Language::CZECH,
            'timeout' => 30,
        ]);
    }

    /**
     * Definuje a vrací seznam poskytovatelů služeb, které jsou registrovány během testování.
     * Tato metoda je využívána pro registraci PaymentInvoicingSystemProvideru, což umožňuje
     * testování integrace poskytovatele s Laravel aplikací.
     */
    protected function getPackageProviders($app): array
    {
        return [PaymentInvoicingSystemProvider::class];
    }
}
