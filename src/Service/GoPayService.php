<?php

declare(strict_types=1);

namespace Hejna\UnifiedPaymentInvoicingSystem\Service;

use GoPay\Http\Response;
use GoPay\Payments;
use Hejna\UnifiedPaymentInvoicingSystem\DTO\UserDTO;
use Hejna\UnifiedPaymentInvoicingSystem\Factory\GoPayFactory;
use Hejna\UnifiedPaymentInvoicingSystem\Interface\PaymentServiceInterface;
use Hejna\UnifiedPaymentInvoicingSystem\Validation\GoPayPaymentValidator;
use Illuminate\Routing\UrlGenerator;

/**
 * Služba GoPayService slouží jako most mezi aplikací a platebním systémem GoPay.
 * Umožňuje vytváření nových plateb, získávání stavů existujících plateb, refundaci plateb,
 * jakož i vytváření, správu a zrušení opakujících se plateb a předautorizací.
 * Implementuje rozhraní PaymentServiceInterface a integruje validaci platebních údajů
 * pomocí GoPayPaymentValidator.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
class GoPayService implements PaymentServiceInterface
{
    /**
     * Konstruktor třídy GoPayService.
     * Inicializuje potřebné závislosti.
     */
    public function __construct(
        private readonly UrlGenerator $urlGenerator,
        private readonly GoPayPaymentValidator $paymentValidator,
    ) {
        $this->paymentValidator->validateConfiguration();
    }

    /**
     * Vytvoří novou platbu s použitím GoPay API.
     */
    public function createPayment(int $orderNumber, UserDTO $user, int $amount, array $products): Response
    {
        $this->paymentValidator->validateEmail($user);
        $this->paymentValidator->validateOrderNumber($orderNumber);
        $this->paymentValidator->validateAmount($amount);
        $this->paymentValidator->validateProducts($products);

        $paymentSettings = config('payment.payment_settings');
        if (! is_array($paymentSettings)) {
            throw new \UnexpectedValueException('Expected payment settings to be an array.');
        }

        $callbackUrls = $paymentSettings['callback_urls'] ?? [];

        $paymentData = [
            'payer' => [
                'default_payment_instrument' => $paymentSettings['default_payment_instrument'],
                'allowed_payment_instruments' => $paymentSettings['allowed_payment_instruments'],
                'default_swift' => $paymentSettings['default_swift'],
                'allowed_swifts' => $paymentSettings['allowed_swifts'],
                'contact' => $this->goPayContactFormat($user),
            ],
            'amount' => $amount * $paymentSettings['amount_multiplier'],
            'currency' => $paymentSettings['currency'],
            'order_number' => $orderNumber,
            'order_description' => $paymentSettings['order_description'],
            'items' => $products,
            'callback' => [
                'return_url' => $this->urlGenerator->route($callbackUrls['return_url']),
                'notification_url' => $this->urlGenerator->route($callbackUrls['notification_url']),
            ],
            'lang' => $paymentSettings['lang'],
        ];

        return $this->clientInit()
            ->createPayment($paymentData);
    }

    /**
     * Získá stav platby z GoPay API.
     */
    public function getPaymentStatus(int $id): Response
    {
        return $this->clientInit()
            ->getStatus($id);
    }

    /**
     * Inicializuje klienta GoPay API s konfigurací načtenou z aplikace.
     */
    public function clientInit(): Payments
    {
        return GoPayFactory::createPayments();
    }

    /**
     * Provádí refundaci platby pomocí GoPay API.
     */
    public function refundPayment(string $id, int $amount): Response
    {
        return $this->clientInit()
            ->refundPayment($id, $amount);
    }

    /**
     * Vytvoří opakovanou platbu na základě existující platby.
     */
    public function createRecurrence(
        string $parentId,
        array $products,
        int $amount,
        UserDTO $user,
        int $orderNumber
    ): Response {
        $this->paymentValidator->validateEmail($user);
        $this->paymentValidator->validateOrderNumber($orderNumber);
        $this->paymentValidator->validateAmount($amount);
        $this->paymentValidator->validateProducts($products);

        $paymentSettings = config('payment.payment_settings');
        if (! is_array($paymentSettings)) {
            throw new \UnexpectedValueException('Expected payment settings to be an array.');
        }

        $paymentData = [
            'payer' => [
                'default_payment_instrument' => $paymentSettings['default_payment_instrument'],
                'allowed_payment_instruments' => $paymentSettings['allowed_payment_instruments'],
                'default_swift' => $paymentSettings['default_swift'],
                'allowed_swifts' => $paymentSettings['allowed_swifts'],
                'contact' => $this->goPayContactFormat($user),
            ],
            'amount' => $amount * $paymentSettings['amount_multiplier'],
            'currency' => $paymentSettings['currency'],
            'order_number' => $orderNumber,
            'order_description' => $paymentSettings['order_description'],
            'items' => $products,
            'lang' => $paymentSettings['lang'],
        ];

        return $this->clientInit()
            ->createRecurrence($parentId, $paymentData);
    }

    /**
     * Zruší opakovanou platbu.
     */
    public function voidRecurrence(string $parentId): Response
    {
        return $this->clientInit()
            ->voidRecurrence($parentId);
    }

    /**
     * Provádí zachycení předautorizované platby.
     */
    public function capturePreauthorizedPayment(string $id): Response
    {
        return $this->clientInit()
            ->captureAuthorization($id);
    }

    /**
     * Zruší předautorizaci platby.
     */
    public function voidPreauthorization(string $id): Response
    {
        return $this->clientInit()
            ->voidAuthorization($id);
    }

    /**
     * Převede informace o uživateli na formát požadovaný GoPay API.
     */
    private function goPayContactFormat(UserDTO $user): array
    {
        return [
            'first_name' => $user->firstName,
            'last_name' => $user->lastName,
            'email' => $user->email,
            'phone_number' => $user->phoneNumber,
            'country_code' => $user->deliveryInformations->countryCode,
        ];
    }
}
