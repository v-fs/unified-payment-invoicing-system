<?php

declare(strict_types=1);

namespace Hejna\UnifiedPaymentInvoicingSystem\Interface;

use GoPay\Http\Response;
use Hejna\UnifiedPaymentInvoicingSystem\DTO\UserDTO;

/**
 * Rozhraní PaymentServiceInterface definuje základní funkcionality pro manipulaci s platebními operacemi v systému.
 * Obsahuje metody pro vytváření plateb, získávání stavu plateb, refundace plateb, zjišťování informací o platebních operacích
 * a manipulaci s opakujícími se platbami a předautorizacemi.
 * Tato rozhraní je implementována třídou, která slouží k integraci s konkrétním platebním systémem.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
interface PaymentServiceInterface
{
    /**
     * Vytvoří novou platební operaci na základě čísla objednávky, uživatelských údajů, částky a produktů.
     *
     * @param int $orderNumber Číslo objednávky.
     * @param UserDTO $user Objekt obsahující údaje o uživateli.
     * @param int $amount Částka platby.
     * @param array $products Pole produktů, za které se platba provádí.
     * @return Response Odpověď na požadavek na vytvoření platby.
     */
    public function createPayment(int $orderNumber, UserDTO $user, int $amount, array $products): Response;

    /**
     * Získá stav platby na základě jejího identifikátoru.
     *
     * @param int $id Identifikátor platby.
     * @return Response Odpověď na požadavek na získání stavu platby.
     */
    public function getPaymentStatus(int $id): Response;

    /**
     * Provede refundaci platby na základě jejího identifikátoru a částky.
     *
     * @param string $id Identifikátor platby.
     * @param int $amount Částka k refundaci.
     * @return Response Odpověď na požadavek na refundaci platby.
     */
    public function refundPayment(string $id, int $amount): Response;

    /**
     * Vytvoří opakující se platbu na základě rodičovského identifikátoru, produktů, částky, uživatelských údajů a čísla objednávky.
     *
     * @param string $parentId Identifikátor rodičovské platby.
     * @param array $products Pole produktů, za které se platba provádí.
     * @param int $amount Částka platby.
     * @param UserDTO $user Objekt obsahující údaje o uživateli.
     * @param int $orderNumber Číslo objednávky.
     * @return Response Odpověď na požadavek na vytvoření opakující se platby.
     */
    public function createRecurrence(
        string $parentId,
        array $products,
        int $amount,
        UserDTO $user,
        int $orderNumber
    ): Response;

    /**
     * Zruší opakující se platbu na základě jejího rodičovského identifikátoru.
     *
     * @param string $parentId Identifikátor rodičovské platby.
     * @return Response Odpověď na požadavek na zrušení opakující se platby.
     */
    public function voidRecurrence(string $parentId): Response;

    /**
     * Provede inkaso předautorizované platby na základě jejího identifikátoru.
     *
     * @param string $id Identifikátor předautorizované platby.
     * @return Response Odpověď na požadavek na inkaso předautorizované platby.
     */
    public function capturePreauthorizedPayment(string $id): Response;

    /**
     * Zruší předautorizaci platby na základě jejího identifikátoru.
     *
     * @param string $id Identifikátor předautorizované platby.
     * @return Response Odpověď na požadavek na zrušení předautorizace platby.
     */
    public function voidPreauthorization(string $id): Response;
}
