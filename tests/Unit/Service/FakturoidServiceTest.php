<?php

declare(strict_types=1);

namespace Tests\Unit\Service;

use Fakturoid\Client;
use Hejna\UnifiedPaymentInvoicingSystem\DTO\DeliveryInformationDTO;
use Hejna\UnifiedPaymentInvoicingSystem\DTO\InvoiceSubjectDTO;
use Hejna\UnifiedPaymentInvoicingSystem\DTO\ProductInvoiceFormatDTO;
use Hejna\UnifiedPaymentInvoicingSystem\DTO\UserDTO;
use Hejna\UnifiedPaymentInvoicingSystem\Enum\InvoiceActionEnum;
use Hejna\UnifiedPaymentInvoicingSystem\Exception\NotExistInvoiceException;
use Hejna\UnifiedPaymentInvoicingSystem\PaymentInvoicingSystemProvider;
use Hejna\UnifiedPaymentInvoicingSystem\Service\FakturoidService;
use Hejna\UnifiedPaymentInvoicingSystem\Validation\FakturoidValidator;
use Illuminate\Support\Carbon;
use Orchestra\Testbench\TestCase;
use stdClass;

class FakturoidServiceTest extends TestCase
{
    public $clientMock;

    public FakturoidValidator $fakturoidValidatorMock;

    private Client $fakturoidClientMock;

    private FakturoidService $fakturoidService;

    protected function setUp(): void
    {
        parent::setUp();
        $this->fakturoidClientMock = $this->createMock(Client::class);
        $this->fakturoidValidatorMock = $this->createMock(FakturoidValidator::class);

        $this->fakturoidService = new class(
            $this->fakturoidValidatorMock,
            $this->fakturoidClientMock
        ) extends FakturoidService
        {
            public function __construct(
                FakturoidValidator $validatorMock,
                private readonly Client $clientMock
            ) {
                parent::__construct($validatorMock);
            }

            public function clientInit(): Client
            {
                return $this->clientMock;
            }
        };
    }

    /**
     * Metoda testCreateSubjectAndMapToDTO testuje správnost vytvoření nového subjektu v Fakturoidu a jeho mapování na DTO objekt.
     */
    public function testCreateSubjectAndMapToDTO(): void
    {
        $user = new UserDTO(
            firstName: 'Test',
            lastName: 'User',
            email: 'test@example.com',
            phoneNumber: '123456789',
            deliveryInformations: new DeliveryInformationDTO(
                'Czech Republic',
                'Test Street',
                '1',
                '12345',
                'Prague',
                'CZE'
            )
        );

        $expectedData = (object) [
            'id' => 123,
            'custom_id' => 'test@example.com',
            'type' => 'customer',
            'name' => 'Test User',
            'street' => 'Test Street, 1',
            'city' => 'Prague',
            'zip' => '12345',
            'country' => 'Czech Republic',
            'full_name' => 'Test User',
            'email' => 'test@example.com',
            'phone' => '123456789',
            'url' => 'https://example.com/subjects/123',
            'created_at' => '2024-03-16T12:00:00Z',
            'updated_at' => '2024-03-16T12:00:00Z',
        ];

        $newSubjectMockResponse = $this->createMock(TestMockResponse::class);
        $newSubjectMockResponse->method('getBody')
            ->willReturn($expectedData);

        $this->fakturoidClientMock->expects($this->once())
            ->method('createSubject')
            ->with(
                $this->equalTo([
                    'name' => 'Test User',
                    'email' => 'test@example.com',
                    'custom_id' => 'test@example.com',
                    'type' => 'customer',
                    'street' => 'Test Street, 1',
                    'zip' => '12345',
                    'city' => 'Prague',
                    'country' => 'Czech Republic',
                    'full_name' => 'Test User',
                    'phone' => '123456789',
                ])
            )
            ->willReturn($newSubjectMockResponse);

        $result = $this->fakturoidService->createSubjectAndMapToDTO($user);

        $this->assertInstanceOf(InvoiceSubjectDTO::class, $result);
        $this->assertEquals($expectedData->id, $result->id);
        $this->assertEquals($expectedData->custom_id, $result->customId);
        $this->assertEquals($expectedData->type, $result->type);

        $expectedDto = new InvoiceSubjectDTO(
            id: 123,
            customId: 'test@example.com',
            type: 'customer',
            name: 'Test User',
            street: 'Test Street, 1',
            street2: null,
            city: 'Prague',
            zip: '12345',
            country: 'Czech Republic',
            registrationNo: null,
            vatNo: null,
            localVatNo: null,
            bankAccount: null,
            iban: null,
            variableSymbol: null,
            enabledReminders: false,
            fullName: 'Test User',
            email: 'test@example.com',
            emailCopy: null,
            phone: '123456789',
            privateNote: null,
            url: 'https://example.com/subjects/123',
            createdAt: '2024-03-16T12:00:00Z',
            updatedAt: '2024-03-16T12:00:00Z'
        );

        $this->assertEquals($expectedDto, $result);
    }

    /**
     * Metoda testCreateOrFindInvoiceSubjectWhenSubjectDoesNotExist testuje vytvoření nového subjektu, když ještě neexistuje v Fakturoidu.
     */
    public function testCreateOrFindInvoiceSubjectWhenSubjectDoesNotExist(): void
    {
        $responseMock = $this->createMock(TestMockResponse::class);
        $responseMock->method('getBody')
            ->willReturn([]);

        $this->fakturoidClientMock->method('getSubjects')
            ->willReturn($responseMock);

        $newSubjectMockResponse = $this->createMock(TestMockResponse::class);
        $newSubjectMockResponse->method('getBody')
            ->willReturn((object) [
                'id' => 123,
                'custom_id' => 'test@example.com',
                'type' => 'customer',
                'name' => 'Daniel',
                'street' => 'Testovací Ulice 123',
                'city' => 'Testovací Město',
                'zip' => '12345',
                'country' => 'Testovací Země',
                'full_name' => 'Daniel Testovací',
                'email' => 'daniel@example.com',
                'url' => 'https://example.com/subjects/123',
                'created_at' => '2024-03-17T12:00:00Z',
                'updated_at' => '2024-03-17T12:00:00Z',
            ]);

        $this->fakturoidClientMock->expects($this->once())
            ->method('createSubject')
            ->willReturn($newSubjectMockResponse);

        $userDTO = new UserDTO(
            firstName: 'Test',
            lastName: 'User',
            email: 'test@example.com',
            phoneNumber: '123456789',
            deliveryInformations: new DeliveryInformationDTO(
                country: 'Česká Republika',
                street: 'Šeříková',
                streetNumber: '5.',
                postalCode: '35002',
                city: 'Cheb',
                countryCode: 'CZE'
            )
        );

        $result = $this->fakturoidService->createOrFindInvoiceSubject($userDTO);

        $this->assertInstanceOf(InvoiceSubjectDTO::class, $result);
    }

    /**
     * Metoda testGetBinaryInvoiceReturnsPdfContent testuje, zda metoda getBinaryInvoice vrátí obsah PDF souboru.
     */
    public function testGetBinaryInvoiceReturnsPdfContent(): void
    {
        $invoiceNumber = '12345';
        $expectedPdfContent = 'PDF content';

        // Místo vytváření JSON řetězce, přímo vytvoříme stdClass objekt
        $responseBody = new stdClass();
        $responseBody->content = $expectedPdfContent;

        // Nyní předáváme $responseBody přímo, jelikož je to instance stdClass
        $responseMock = new TestMockResponse(200, $responseBody);

        // Mock setup
        $this->fakturoidClientMock->method('getInvoicePdf')
            ->with($invoiceNumber)
            ->willReturn($responseMock);

        // Volání testované metody
        $result = $this->fakturoidService->getBinaryInvoice($invoiceNumber);

        // Protože očekáváme, že výsledek bude řetězec, předpokládejme, že metoda ve skutečnosti
        // dekóduje JSON zpět na řetězec (příklad logiky, která by mohla být ve vaší službě)
        // Pro účely tohoto testu, předpokládejme, že chceme zkontrolovat samotný obsah
        $decodedResult = json_decode($result);
        $this->assertEquals($expectedPdfContent, $decodedResult->content);
    }

    /**
     * Metoda testGetBinaryInvoiceThrowsExceptionIfInvoiceDoesNotExist testuje, zda metoda getBinaryInvoice vyhodí výjimku, pokud faktura neexistuje.
     */
    public function testGetBinaryInvoiceThrowsExceptionIfInvoiceDoesNotExist(): void
    {
        $this->clientMock = $this->createMock(Client::class);

        $this->expectException(NotExistInvoiceException::class);
        $this->expectExceptionMessage('Invoice with number 12345 does not exist.');

        $invoiceNumber = '12345';
        $expectedPdfContent = new \stdClass();
        $expectedPdfContent->body = 'PDF content';

        $responseMock = new TestMockResponse(404, $expectedPdfContent);

        $this->clientMock->method('getInvoicePdf')
            ->with($invoiceNumber)
            ->willReturn($responseMock);

        $this->fakturoidService->getBinaryInvoice($invoiceNumber);
    }

    /**
     * Metoda testGetBinaryInvoiceThrowsExceptionOnFailure testuje, zda metoda getBinaryInvoice vyhodí výjimku při chybě při získávání faktury.
     */
    public function testGetBinaryInvoiceThrowsExceptionOnFailure(): void
    {
        $this->clientMock = $this->createMock(Client::class);

        $this->expectException(NotExistInvoiceException::class);
        $this->expectExceptionMessage('Failed to retrieve invoice:');

        $invoiceNumber = '12345';

        $this->clientMock->method('getInvoicePdf')
            ->with($invoiceNumber)
            ->willThrowException(new \RuntimeException('Some error'));

        $this->fakturoidService->getBinaryInvoice($invoiceNumber);
    }

    /**
     * Metoda testCreateOrFindInvoiceSubjectSubjectExists testuje hledání nebo vytvoření subjektu, když již existuje v Fakturoidu.
     */
    public function testCreateOrFindInvoiceSubjectSubjectExists(): void
    {
        $responseMock = $this->createMock(TestMockResponse::class);

        $responseMock->method('getBody')
            ->willReturn([
                (object) [
                    'id' => 19_995_714,
                    'custom_id' => 'hejna@yeetzone.com',
                    'type' => 'customer',
                    'name' => '35002 35002',
                    'street' => '35002, 35002',
                    'city' => '35002',
                    'zip' => '35002',
                    'country' => 'CZ',
                    'enabled_reminders' => true,
                    'full_name' => '35002 35002',
                    'email' => 'hejna@yeetzone.com',
                    'phone' => '774416609',
                    'url' => 'https://app.fakturoid.cz/api/v2/accounts/milosouhrabka/subjects/19995714.json',
                    'created_at' => '2024-02-08T13:03:56.027+01:00',
                    'updated_at' => '2024-02-21T23:27:08.511+01:00',
                ],
            ]);

        $this->fakturoidClientMock->method('getSubjects')
            ->willReturn($responseMock);

        $userDTO = new UserDTO(
            firstName: 'Test',
            lastName: 'User',
            email: 'hejna@yeetzone.com',
            phoneNumber: '774416609',
            deliveryInformations: new DeliveryInformationDTO(
                'Česká Republika',
                'Šeříková',
                '5.',
                '35002',
                'Cheb',
                'CZE'
            )
        );

        $result = $this->fakturoidService->createOrFindInvoiceSubject($userDTO);

        $this->assertInstanceOf(InvoiceSubjectDTO::class, $result);
        $this->assertEquals(19_995_714, $result->id);
        $this->assertEquals('hejna@yeetzone.com', $result->email);

        $this->assertEquals('hejna@yeetzone.com', $result->customId);
        $this->assertEquals('customer', $result->type);
        $this->assertEquals('35002 35002', $result->name);
        $this->assertEquals('35002, 35002', $result->street);
        $this->assertEquals('35002', $result->city);
        $this->assertEquals('35002', $result->zip);
        $this->assertEquals('CZ', $result->country);
        $this->assertTrue($result->enabledReminders);
        $this->assertEquals('35002 35002', $result->fullName);
        $this->assertEquals('774416609', $result->phone);
        $this->assertEquals(
            'https://app.fakturoid.cz/api/v2/accounts/milosouhrabka/subjects/19995714.json',
            $result->url
        );
        $this->assertEquals('2024-02-08T13:03:56.027+01:00', $result->createdAt);
        $this->assertEquals('2024-02-21T23:27:08.511+01:00', $result->updatedAt);
    }

    /**
     * Metoda testUpdateInvoice testuje aktualizaci faktury pomocí služby Fakturoid.
     */
    public function testCreateInvoice(): void
    {
        $productDTOs = [new ProductInvoiceFormatDTO('Život je tanec (Audiokniha)', 1, 499.0, 0.0, 499.0)];

        $date = Carbon::now();
        $formattedDate = $date->format('Y-m-d H:i:s');

        // Převod ProductInvoiceFormatDTO na pole polí pro očekávaný payload
        $productsArray = array_map(static fn($productDTO) => $productDTO->toArray(), $productDTOs);

        $expectedPayload = [
            'subject_id' => 19_995_714,
            'lines' => $productsArray,
            'issued_on' => $formattedDate,
            'due_on' => $formattedDate,
            'sent_at' => $formattedDate,
            'paid_at' => $formattedDate,
            'accepted_at' => $formattedDate,
        ];

        $invoiceTestMockResponse = $this->createMock(TestMockResponse::class);

        $responseBody = new \stdClass();
        $responseBody->id = 35_409_683;
        $invoiceTestMockResponse->method('getBody')
            ->willReturn($responseBody);

        $this->fakturoidClientMock->expects($this->once())
            ->method('createInvoice')
            ->with($this->equalTo($expectedPayload))
            ->willReturn($invoiceTestMockResponse);

        $event = InvoiceActionEnum::PAY;
        $this->fakturoidClientMock->expects($this->once())
            ->method('fireInvoice')
            ->with($this->equalTo($responseBody->id), $this->equalTo($event->value));

        $result = $this->fakturoidService->createInvoice($productDTOs, 19_995_714, $event, $date);

        $this->assertEquals($responseBody->id, $result);
    }

    /**
     * Testuje vytvoření faktury s automatickým nastavením aktuálního data, pokud není datum explicitně poskytnuto.
     */
    public function testCreateInvoiceWithAutomaticCurrentDate(): void
    {
        Carbon::setTestNow(Carbon::create(2024, 3, 15, 20, 37, 32));

        $productDTOs = [new ProductInvoiceFormatDTO('Život je tanec (Audiokniha)', 1, 499.0, 0.0, 499.0)];

        $expectedDate = '2024-03-15 20:37:32';

        $productsArray = array_map(static fn($productDTO) => $productDTO->toArray(), $productDTOs);

        $expectedPayload = [
            'subject_id' => 19_995_714,
            'lines' => $productsArray,
            'issued_on' => $expectedDate,
            'due_on' => $expectedDate,
            'sent_at' => $expectedDate,
            'paid_at' => $expectedDate,
            'accepted_at' => $expectedDate,
        ];

        $invoiceTestMockResponse = $this->createMock(TestMockResponse::class);

        $responseBody = new \stdClass();
        $responseBody->id = 35_409_683;
        $invoiceTestMockResponse->method('getBody')
            ->willReturn($responseBody);

        $this->fakturoidClientMock->expects($this->once())
            ->method('createInvoice')
            ->with($this->equalTo($expectedPayload))
            ->willReturn($invoiceTestMockResponse);

        $event = InvoiceActionEnum::PAY;
        $this->fakturoidClientMock->expects($this->once())
            ->method('fireInvoice')
            ->with($this->equalTo($responseBody->id), $this->equalTo($event->value));

        $result = $this->fakturoidService->createInvoice($productDTOs, 19_995_714, $event);

        $this->assertEquals($responseBody->id, $result);

        Carbon::setTestNow();
    }

    /**
     * Metoda testUpdateInvoice testuje aktualizaci faktury pomocí služby Fakturoid.
     */
    public function testUpdateInvoice(): void
    {
        $this->fakturoidClientMock->expects($this->once())
            ->method('updateInvoice');

        $result = $this->fakturoidService->updateInvoice(456, '2022-01-02');

        $this->assertEquals(1, $result);
    }

    /**
     * Metoda testClientInitShouldReturnConfiguredFakturoidClient testuje, zda metoda clientInit vrací konfigurovaného klienta pro Fakturoid.
     */
    public function testClientInitShouldReturnConfiguredFakturoidClient(): void
    {
        $fakturoidService = $this->app->make(FakturoidService::class);
        $client = $fakturoidService->clientInit();
        $this->assertNotNull($client);
    }

    protected function getEnvironmentSetUp($app): void
    {
        $app['config']->set('invoice.client_init', [
            'invoiceUsername' => 'test_username',
            'invoiceEmail' => 'test_email@example.com',
            'invoiceApiKey' => 'test_apikey',
            'invoiceUserAgent' => 'test_useragent',
        ]);
    }

    protected function getPackageProviders($app): array
    {
        return [PaymentInvoicingSystemProvider::class];
    }
}
