<?php

declare(strict_types=1);

namespace Tests\Unit\Service;

use GoPay\Definition\Language;
use GoPay\Definition\Payment\BankSwiftCode;
use GoPay\Definition\Payment\Currency;
use GoPay\Definition\Payment\PaymentInstrument;
use GoPay\Definition\TokenScope;
use GoPay\Http\Response;
use GoPay\Payments;
use Hejna\UnifiedPaymentInvoicingSystem\DTO\DeliveryInformationDTO;
use Hejna\UnifiedPaymentInvoicingSystem\DTO\UserDTO;
use Hejna\UnifiedPaymentInvoicingSystem\Service\GoPayService;
use Hejna\UnifiedPaymentInvoicingSystem\Validation\GoPayPaymentValidator;
use Illuminate\Routing\UrlGenerator;
use Orchestra\Testbench\TestCase;

/**
 * Testuje funkčnost třídy GoPayService a její integraci s externími službami.
 * Obsahuje metody pro vytváření plateb, získávání stavu plateb, refundace plateb, zjišťování informací o platebních operacích
 * a manipulaci s opakujícími se platbami a předautorizacemi. Tato třída slouží k integraci s konkrétním platebním systémem.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
class GoPayServiceTest extends TestCase
{
    private Payments $paymentsMock;

    private GoPayPaymentValidator $paymentValidatorMock;

    private GoPayService $goPayService;

    /**
     * Nastavuje prostředí pro každý test před jeho spuštěním.
     * Inicializuje validátor GoPayPayment pro použití v testech, což zahrnuje
     * přípravu potřebných objektů a konfigurací před každým testem.
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->paymentsMock = $this->createMock(Payments::class);

        $urlGeneratorMock = $this->createMock(UrlGenerator::class);

        $this->paymentValidatorMock = $this->createMock(GoPayPaymentValidator::class);

        $this->goPayService = $this->getMockBuilder(GoPayService::class)
            ->setConstructorArgs([$urlGeneratorMock, $this->paymentValidatorMock])
            ->onlyMethods(['clientInit'])
            ->getMock();

        $this->goPayService->method('clientInit')
            ->willReturn($this->paymentsMock);
    }

    /**
     * Testuje vytvoření platby a ověřuje, že odpověď odpovídá očekávané mock odpovědi.
     */
    public function testCreatePayment(): void
    {
        $responseMock = $this->createMock(Response::class);
        $this->paymentsMock->method('createPayment')
            ->willReturn($responseMock);

        $this->goPayService->method('clientInit')
            ->willReturn($this->paymentsMock);

        $userDTO = new UserDTO(
            firstName: 'Test',
            lastName: 'User',
            email: 'hejna@yeetzone.com',
            phoneNumber: '774416609',
            deliveryInformations: new DeliveryInformationDTO(
                'Česká Republika',
                'Šeříková',
                '5.',
                '35002',
                'Cheb',
                'CZE'
            )
        );

        $response = $this->goPayService->createPayment(123, $userDTO, 1000, []);

        $this->assertSame($responseMock, $response);
    }

    /**
     * Testuje vytvoření opakované platby a ověřuje správné zacházení s parametry.
     */
    public function testCreateRecurrence(): void
    {
        $parentId = 'parentIdExample';
        $amount = 1000;
        $orderNumber = 123;
        $userDTO = new UserDTO(
            firstName: 'Test',
            lastName: 'User',
            email: 'hejna@yeetzone.com',
            phoneNumber: '774416609',
            deliveryInformations: new DeliveryInformationDTO(
                'Česká Republika',
                'Šeříková',
                '5.',
                '35002',
                'Cheb',
                'CZE'
            )
        );

        $products = [];
        $responseMock = $this->createMock(Response::class);

        $this->paymentsMock->expects($this->once())
            ->method('createRecurrence')
            ->with(
                $this->equalTo($parentId),
                $this->callback(static fn ($subject) => is_array($subject) &&
                    $subject['amount'] === $amount * 100 &&
                    $subject['order_number'] === $orderNumber &&
                    $subject['items'] === $products)
            )
            ->willReturn($responseMock);

        $this->paymentValidatorMock->expects($this->once())
            ->method('validateEmail')
            ->with($this->equalTo($userDTO));
        $this->paymentValidatorMock->expects($this->once())
            ->method('validateOrderNumber')
            ->with($this->equalTo($orderNumber));
        $this->paymentValidatorMock->expects($this->once())
            ->method('validateAmount')
            ->with($this->equalTo($amount));
        $this->paymentValidatorMock->expects($this->once())
            ->method('validateProducts')
            ->with($this->equalTo($products));

        $result = $this->goPayService->createRecurrence($parentId, $products, $amount, $userDTO, $orderNumber);
        $this->assertSame($responseMock, $result);
    }

    /**
     * Ověřuje, že `clientInit` správně inicializuje a vrací konfigurovanou instanci `Payments`.
     */
    public function testClientInitReturnsConfiguredGoPayClient(): void
    {
        $urlGeneratorMock = $this->createMock(UrlGenerator::class);
        $paymentValidatorMock = $this->createMock(GoPayPaymentValidator::class);

        $goPayService = new GoPayService($urlGeneratorMock, $paymentValidatorMock);

        $payments = $goPayService->clientInit();
        $this->assertInstanceOf(Payments::class, $payments);
    }

    /**
     * Testuje vyhození výjimky `UnexpectedValueException` při neplatném konfiguračním nastavení plateb.
     */
    public function testCreatePaymentThrowsExceptionWhenPaymentSettingsNotArray(): void
    {
        $userDTO = $this->setUpTestEnvironmentForPaymentSettingsException();
        $orderNumber = 123;
        $amount = 1000;
        $products = [];

        $this->expectException(\UnexpectedValueException::class);
        $this->expectExceptionMessage('Expected payment settings to be an array.');

        $this->goPayService->createPayment($orderNumber, $userDTO, $amount, $products);
    }

    /**
     * Testuje vyhození výjimky `UnexpectedValueException` při vytváření opakované platby s neplatným konfiguračním nastavením.
     */
    public function testCreateRecurrenceThrowsExceptionWhenPaymentSettingsNotArray(): void
    {
        $userDTO = $this->setUpTestEnvironmentForPaymentSettingsException();
        $parentId = 'parentIdExample';
        $orderNumber = 123;
        $amount = 1000;
        $products = [];

        $this->expectException(\UnexpectedValueException::class);
        $this->expectExceptionMessage('Expected payment settings to be an array.');

        $this->goPayService->createRecurrence($parentId, $products, $amount, $userDTO, $orderNumber);
    }

    /**
     * Testuje funkčnost získání stavu platby.
     */
    public function testGetPaymentStatus(): void
    {
        $paymentId = 123;
        $response = new Response();

        $this->paymentsMock->expects($this->once())
            ->method('getStatus')
            ->with($paymentId)
            ->willReturn($response);

        $result = $this->goPayService->getPaymentStatus($paymentId);

        $this->assertSame($response, $result);
    }

    /**
     * Ověřuje, že metoda `clientInit` konfiguruje a vrací instanci klienta GoPay.
     */
    public function clientInitShouldReturnConfiguredGoPayClient(): void
    {
        $goPayService = $this->app->make(GoPayService::class);
        $client = $goPayService->clientInit();

        $this->assertNotNull($client);
    }

    /**
     * Testuje funkčnost zrušení opakované platby.
     */
    public function testVoidRecurrence(): void
    {
        $parentId = 'exampleParentId';
        $responseMock = $this->createMock(Response::class);

        $this->paymentsMock->expects($this->once())
            ->method('voidRecurrence')
            ->with($parentId)
            ->willReturn($responseMock);

        $result = $this->goPayService->voidRecurrence($parentId);

        $this->assertSame($responseMock, $result);
    }

    /**
     * Testuje funkčnost zachycení předautorizované platby.
     */
    public function testCapturePreauthorizedPayment(): void
    {
        $paymentId = 'examplePaymentId';
        $responseMock = $this->createMock(Response::class);

        $this->paymentsMock->expects($this->once())
            ->method('captureAuthorization')
            ->with($paymentId)
            ->willReturn($responseMock);

        $result = $this->goPayService->capturePreauthorizedPayment($paymentId);

        $this->assertSame($responseMock, $result);
    }

    /**
     * Testuje funkčnost zrušení předautorizace platby.
     */
    public function testVoidPreauthorization(): void
    {
        $paymentId = 'examplePaymentId';
        $responseMock = $this->createMock(Response::class);

        $this->paymentsMock->expects($this->once())
            ->method('voidAuthorization')
            ->with($paymentId)
            ->willReturn($responseMock);

        $result = $this->goPayService->voidPreauthorization($paymentId);

        $this->assertSame($responseMock, $result);
    }

    /**
     * Testuje funkčnost refundace platby.
     */
    public function testRefundPayment(): void
    {
        $id = 'examplePaymentId';
        $amount = 1000;
        $responseMock = $this->createMock(Response::class);

        $this->paymentsMock->expects($this->once())
            ->method('refundPayment')
            ->with($this->equalTo($id), $this->equalTo($amount))
            ->willReturn($responseMock);

        $result = $this->goPayService->refundPayment($id, $amount);

        $this->assertSame(
            $responseMock,
            $result,
            'The refundPayment method did not return the expected response object.'
        );
    }

    /**
     * Nastavuje testovací prostředí pro testovanou aplikaci.
     * Konfiguruje klienta GoPay s testovacími hodnotami, které umožňují simulaci
     * platebních operací bez nutnosti reálného připojení k platební bráně.
     */
    protected function getEnvironmentSetUp($app): void
    {
        $app['config']->set('payment.client_init', [
            'goid' => '123456789',
            'clientId' => 'test_client_id',
            'clientSecret' => 'test_client_secret',
            'gatewayUrl' => 'https://test-gateway.gopay.com',
            'scope' => TokenScope::ALL,
            'language' => Language::CZECH,
            'timeout' => 30,
        ]);
        $app['config']->set('payment.payment_settings', [
            'default_payment_instrument' => PaymentInstrument::PAYMENT_CARD,
            'allowed_payment_instruments' => [PaymentInstrument::PAYMENT_CARD],
            'default_swift' => BankSwiftCode::KOMERCNI_BANKA,
            'allowed_swifts' => [
                BankSwiftCode::FIO_BANKA,
                BankSwiftCode::MBANK,
                BankSwiftCode::KOMERCNI_BANKA,
                BankSwiftCode::CSOB,
                BankSwiftCode::RAIFFEISENBANK,
            ],
            'currency' => Currency::CZECH_CROWNS,
            'order_description' => 'Objednávka z webu',
            'lang' => Language::CZECH,
            'amount_multiplier' => 100,
            'callback_urls' => [
                'return_url' => 'orderStatus',
                'notification_url' => 'orderNotification',
            ],
        ]);
    }

    /**
     * Připravuje testovací prostředí simulující chybnou konfiguraci platebních nastavení.
     * Vrací instanci UserDTO pro použití v testech.
     *
     * @return UserDTO Připravená instance UserDTO pro testování.
     */
    protected function setUpTestEnvironmentForPaymentSettingsException(): UserDTO
    {
        config([
            'payment.payment_settings' => null,
        ]);

        return new UserDTO(
            firstName: 'Test',
            lastName: 'User',
            email: 'hejna@yeetzone.com',
            phoneNumber: '774416609',
            deliveryInformations: new DeliveryInformationDTO(
                'Česká Republika',
                'Šeříková',
                '5.',
                '35002',
                'Cheb',
                'CZE'
            )
        );
    }
}
