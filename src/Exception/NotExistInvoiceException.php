<?php

declare(strict_types=1);

namespace Hejna\UnifiedPaymentInvoicingSystem\Exception;

use Exception;

/**
 * Výjimka NotExistInvoiceException je vyvolána, když faktura neexistuje v systému.
 * Tato výjimka dědí z obecné třídy Exception.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
class NotExistInvoiceException extends Exception
{
}
