<?php

declare(strict_types=1);

namespace Tests\Unit\DTO;

use Hejna\UnifiedPaymentInvoicingSystem\DTO\ProductInvoiceFormatDTO;
use PHPUnit\Framework\TestCase;

/**
 * Testovací třída pro ProductInvoiceFormatDTO.
 * Ověřuje správnou funkčnost a chování DTO pro formát produktu na faktuře.
 * Zahrnuje testy pro ověření, že data předaná do DTO jsou správně nastavena a mohou být
 * korektně získána prostřednictvím metody toArray, jakož i přímý přístup k jednotlivým vlastnostem.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
class ProductInvoiceFormatDTOTest extends TestCase
{
    /**
     * Testuje, že metoda toArray vrací očekávané pole hodnot.
     * Ověřuje, že všechny vlastnosti objektu jsou správně převedeny na pole.
     */
    public function testToArray(): void
    {
        $name = 'Product Name';
        $quantity = 2;
        $unitPrice = 100.0;
        $vatRate = 20.0;
        $unitPriceWithVat = 120.0;

        $dto = new ProductInvoiceFormatDTO(
            name: $name,
            quantity: $quantity,
            unitPrice: $unitPrice,
            vatRate: $vatRate,
            unitPriceWithVat: $unitPriceWithVat
        );

        $expected = [
            'name' => $name,
            'quantity' => $quantity,
            'unit_price' => $unitPrice,
            'vat_rate' => $vatRate,
            'unit_price_with_vat' => $unitPriceWithVat,
        ];

        $this->assertEquals($expected, $dto->toArray(), 'The toArray method did not return the expected array.');
        $this->assertEquals($name, $dto->name, 'The name property is not correctly assigned.');
        $this->assertEquals($quantity, $dto->quantity, 'The quantity property is not correctly assigned.');
        $this->assertEquals($unitPrice, $dto->unitPrice, 'The unitPrice property is not correctly assigned.');
        $this->assertEquals($vatRate, $dto->vatRate, 'The vatRate property is not correctly assigned.');
        $this->assertEquals(
            $unitPriceWithVat,
            $dto->unitPriceWithVat,
            'The unitPriceWithVat property is not correctly assigned.'
        );
    }
}
