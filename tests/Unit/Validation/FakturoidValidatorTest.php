<?php

declare(strict_types=1);

namespace Tests\Unit\Validation;

use Hejna\UnifiedPaymentInvoicingSystem\DTO\ProductInvoiceFormatDTO;
use Hejna\UnifiedPaymentInvoicingSystem\Validation\FakturoidValidator;
use Orchestra\Testbench\TestCase;

/**
 * Tato třída obsahuje testy pro ověření validace konfiguračních nastavení a dat produktů specifických pro Fakturoid.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
class FakturoidValidatorTest extends TestCase
{
    private FakturoidValidator $validator;

    /**
     * Příprava testovacího prostředí před každým testem.
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->validator = new FakturoidValidator();
    }

    /**
     * Testuje vyhození výjimky při chybějícím konfiguračním klíči v nastavení Fakturoidu.
     */
    public function testValidateConfigurationMissingConfig(): void
    {
        // Nastavení prázdné konfigurace pro simulaci chybějícího nastavení
        config([
            'invoice.client_init.invoiceUsername' => null,
        ]);

        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage(
            "The configuration value for 'invoiceUsername' must be provided in the invoice settings."
        );

        $this->validator->validateConfiguration();
    }

    /**
     * Testuje vyhození výjimky při prázdném poli produktů.
     */
    public function testValidateProductsEmptyArray(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Products array cannot be empty.');

        $this->validator->validateProducts([]);
    }

    /**
     * Testuje vyhození výjimky při neplatném produktu v poli produktů.
     */
    public function testValidateProductsInvalidProduct(): void
    {
        $invalidProduct = new \stdClass(); // Příklad neplatného produktu
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('All products must be instances of ProductInvoiceFormatDTO.');

        $this->validator->validateProducts([$invalidProduct]);
    }

    /**
     * Testuje úspěšnou validaci při validních produktech.
     */
    public function testValidateProductsValidProduct(): void
    {
        $validProduct = new ProductInvoiceFormatDTO('Example Product', 1, 100.0, 20.0, 120.0);
        $this->validator->validateProducts([$validProduct]);
        $this->assertTrue(true, 'Valid products pass validation.');
    }
}
