<?php

declare(strict_types=1);

namespace Hejna\UnifiedPaymentInvoicingSystem\DTO;

/**
 * Třída DeliveryInformationDTO představuje DTO objekt obsahující informace o dodací adrese.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
readonly class DeliveryInformationDTO
{
    public function __construct(
        public string $country,
        public string $street,
        public string $streetNumber,
        public string $postalCode,
        public string $city,
        public string $countryCode,
    ) {
    }

    /**
     * Převede objekt DeliveryInformationDTO na asociativní pole.
     */
    public function toArray(): array
    {
        return [
            'country' => $this->country,
            'street' => $this->street,
            'streetNumber' => $this->streetNumber,
            'postalCode' => $this->postalCode,
            'city' => $this->city,
            'countryCode' => $this->countryCode,
        ];
    }
}
