<?php

declare(strict_types=1);

namespace Hejna\UnifiedPaymentInvoicingSystem\Validation;

use Hejna\UnifiedPaymentInvoicingSystem\DTO\ProductPaymentFormatDTO;
use Hejna\UnifiedPaymentInvoicingSystem\DTO\UserDTO;

/**
 * Třída GoPayPaymentValidator slouží k validaci konfiguračních nastavení a dat specifických pro integraci s GoPay.
 * Obsahuje metody pro ověření platby e-mailu, čísla objednávky, množství a formátu produktů, aby bylo zajištěno,
 * že veškerá data splňují požadavky GoPay API a formát pro platby.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta: Ekonomických studií, Vysoká škola finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
class GoPayPaymentValidator
{
    /**
     * Ověřuje platnost e-mailové adresy uživatele.
     *
     * @param UserDTO $user Uživatelský objekt obsahující e-mailovou adresu.
     */
    public function validateEmail(UserDTO $user): void
    {
        if ($user->email === '' || $user->email === '0') {
            throw new \InvalidArgumentException('User email cannot be empty.');
        }
    }

    /**
     * Ověřuje platnost čísla objednávky.
     *
     * @param int $orderNumber Číslo objednávky.
     */
    public function validateOrderNumber(int $orderNumber): void
    {
        if ($orderNumber <= 0) {
            throw new \InvalidArgumentException('Order number must be greater than 0.');
        }
    }

    /**
     * Ověřuje platnost částky.
     *
     * @param int $amount Částka.
     */
    public function validateAmount(int $amount): void
    {
        if ($amount <= 0) {
            throw new \InvalidArgumentException('Amount must be greater than 0.');
        }
    }

    /**
     * Ověřuje platnost pole produktů.
     *
     * @param array $products Pole produktů.
     */
    public function validateProducts(array $products): void
    {
        if ($products === []) {
            throw new \InvalidArgumentException('Products array cannot be empty.');
        }

        foreach ($products as $product) {
            if (! $product instanceof ProductPaymentFormatDTO) {
                throw new \InvalidArgumentException('Each product must be an instance of ProductPaymentFormatDTO.');
            }
        }
    }

    /**
     * Ověřuje platnost konfigurace GoPay.
     */
    public function validateConfiguration(): void
    {
        $requiredConfigKeys = ['goid', 'clientId', 'clientSecret'];
        foreach ($requiredConfigKeys as $key) {
            $value = config('payment.client_init.'.$key);
            if (empty($value)) {
                throw new \InvalidArgumentException(
                    sprintf("The configuration value for '%s' must be provided in the gopay config.", $key)
                );
            }
        }
    }
}
