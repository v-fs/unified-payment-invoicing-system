<?php

declare(strict_types=1);

namespace Hejna\UnifiedPaymentInvoicingSystem\Validation;

use Hejna\UnifiedPaymentInvoicingSystem\DTO\ProductInvoiceFormatDTO;

class FakturoidValidator
{
    /**
     * Validates that all required configuration keys for Fakturoid v3 are provided.
     */
    public function validateConfiguration(): void
    {
        $requiredConfigKeys = ['clientId', 'clientSecret', 'invoiceUserAgent'];

        foreach ($requiredConfigKeys as $key) {
            $value = config('invoice.client_init.' . $key);

            if (empty($value)) {
                throw new \InvalidArgumentException(
                    sprintf("The configuration value for '%s' must be provided in the invoice settings.", $key)
                );
            }
        }
    }

    /**
     * Validates that the products array is not empty and all products are instances of ProductInvoiceFormatDTO.
     *
     * @param array $products Array of products to validate.
     */
    public function validateProducts(array $products): void
    {
        if (empty($products)) {
            throw new \InvalidArgumentException('Products array cannot be empty.');
        }

        foreach ($products as $product) {
            if (!$product instanceof ProductInvoiceFormatDTO) {
                throw new \InvalidArgumentException('All products must be instances of ProductInvoiceFormatDTO.');
            }
        }
    }
}
