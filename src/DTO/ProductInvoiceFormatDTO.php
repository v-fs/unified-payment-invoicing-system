<?php

declare(strict_types=1);

namespace Hejna\UnifiedPaymentInvoicingSystem\DTO;

/**
 * Třída ProductInvoiceFormatDTO představuje DTO objekt obsahující informace o formátu produktu na faktuře.
 * Tento objekt slouží k reprezentaci produktů na fakturách a jejich cenových údajů.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
class ProductInvoiceFormatDTO
{
    public function __construct(
        public string $name,
        public int $quantity,
        public float $unitPrice,
        public float $vatRate,
        public float $unitPriceWithVat,
        public string $unit = "ks",
        public string $priceType = "with_vat",
    ) {
    }

    /**
     * Převede objekt ProductInvoiceFormatDTO na asociativní pole.
     */
    public function toArray(): array
    {
        return [
            'name' => $this->name,
            'quantity' => $this->quantity,
            'unit_price' => $this->unitPrice,
            'vat_rate' => $this->vatRate,
            'unit_price_with_vat' => $this->unitPriceWithVat,
            'unit' => $this->unit,
            'price_type' => $this->priceType,
        ];
    }
}
