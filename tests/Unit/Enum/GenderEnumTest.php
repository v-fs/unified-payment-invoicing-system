<?php

declare(strict_types=1);

namespace Tests\Unit\Enum;

use Hejna\UnifiedPaymentInvoicingSystem\Enum\Gender;
use PHPUnit\Framework\TestCase;

/**
 * Testovací třída pro Gender enum.
 * Ověřuje správnost a funkčnost definovaných hodnot enumu Gender a jejich použití.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
class GenderEnumTest extends TestCase
{
    /**
     * Testuje, že hodnoty enumu Gender jsou správně přiřazeny.
     * Ověřuje očekávané stringové hodnoty pro jednotlivé případy enumu.
     */
    public function testGenderEnumCases(): void
    {
        $this->assertEquals('male', Gender::MALE->value);
        $this->assertEquals('female', Gender::FEMALE->value);
    }

    /**
     * Testuje instanci enumu Gender.
     * Ověřuje, že objekty vytvořené z enum hodnot jsou skutečně instancemi enumu Gender.
     */
    public function testEnumInstantiation(): void
    {
        $male = Gender::MALE;
        $female = Gender::FEMALE;

        $this->assertTrue($male instanceof Gender);
        $this->assertTrue($female instanceof Gender);
    }

    /**
     * Testuje získání všech hodnot enumu Gender.
     * Ověřuje, že metoda cases() vrací správný počet definovaných případů a že obsahuje očekávané hodnoty.
     */
    public function testEnumValues(): void
    {
        $values = Gender::cases();

        $this->assertCount(2, $values);
        $this->assertContains(Gender::MALE, $values);
        $this->assertContains(Gender::FEMALE, $values);
    }
}
