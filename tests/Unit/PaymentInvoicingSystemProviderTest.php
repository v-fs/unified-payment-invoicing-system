<?php

declare(strict_types=1);

namespace Tests\Unit;

use GoPay\Definition\Language;
use GoPay\Definition\TokenScope;
use Hejna\UnifiedPaymentInvoicingSystem\Interface\InvoiceServiceInterface;
use Hejna\UnifiedPaymentInvoicingSystem\Interface\PaymentServiceInterface;
use Hejna\UnifiedPaymentInvoicingSystem\PaymentInvoicingSystemProvider;
use Hejna\UnifiedPaymentInvoicingSystem\Service\FakturoidService;
use Hejna\UnifiedPaymentInvoicingSystem\Service\GoPayService;
use Hejna\UnifiedPaymentInvoicingSystem\Validation\GoPayPaymentValidator;
use Orchestra\Testbench\TestCase;

/**
 * Testuje funkčnost PaymentInvoicingSystemProvideru a jeho integrace s konfigurací.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Tento test ověřuje správnou registraci a konfiguraci služeb platebního a fakturačního systému
 * v rámci Laravel aplikace. Zaměřuje se především na ověření, že služby GoPay a Fakturoid jsou
 * správně zaregistrovány a konfigurovány s předpokládanými hodnotami pro testovací prostředí.
 *
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
class PaymentInvoicingSystemProviderTest extends TestCase
{
    /**
     * Ověřuje, že služby definované v PaymentInvoicingSystemProvideru jsou správně zaregistrovány v Laravel aplikaci.
     */
    public function testServiceRegistration(): void
    {
        $this->assertTrue($this->app->bound(GoPayPaymentValidator::class));

        $invoiceService = $this->app->make(InvoiceServiceInterface::class);
        $this->assertInstanceOf(FakturoidService::class, $invoiceService);

        $paymentService = $this->app->make(PaymentServiceInterface::class);
        $this->assertInstanceOf(GoPayService::class, $paymentService);
    }

    /**
     * Ověřuje, že PaymentInvoicingSystemProvider správně publikuje a nastavuje konfigurační soubory.
     */
    public function testConfigurationPublishing(): void
    {
        // Simulace volání metody boot() pro publikování konfiguračních souborů
        $provider = new PaymentInvoicingSystemProvider($this->app);
        $provider->boot();

        // Ověření, že konfigurační hodnoty jsou dostupné
        $clientInitConfig = $this->app['config']->get('payment.client_init');
        $this->assertNotNull($clientInitConfig);
        $this->assertEquals('123456789', $clientInitConfig['goid']);
    }

    /**
     * Nastaví testovací prostředí a konfigurace pro GoPay klienta.
     */
    protected function getEnvironmentSetUp($app): void
    {
        $app['config']->set('payment.client_init', [
            'goid' => '123456789',
            'clientId' => 'test_client_id',
            'clientSecret' => 'test_client_secret',
            'gatewayUrl' => 'https://test-gateway.gopay.com',
            'scope' => TokenScope::ALL,
            'language' => Language::CZECH,
            'timeout' => 30,
        ]);

        $app['config']->set('invoice.client_init', [
            'invoiceUsername' => 'test_username',
            'invoiceEmail' => 'test_email@example.com',
            'invoiceApiKey' => 'test_apikey',
            'invoiceUserAgent' => 'test_useragent',
        ]);
    }

    /**
     * Definuje a vrací seznam poskytovatelů služeb, které jsou registrovány během testování.
     * Tato metoda je využívána pro registraci PaymentInvoicingSystemProvideru, což umožňuje
     * testování integrace poskytovatele s Laravel aplikací.
     */
    protected function getPackageProviders($app): array
    {
        return [PaymentInvoicingSystemProvider::class];
    }
}
