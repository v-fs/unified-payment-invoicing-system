<?php

declare(strict_types=1);

namespace Tests\Unit\DTO;

use Hejna\UnifiedPaymentInvoicingSystem\DTO\DeliveryInformationDTO;
use Hejna\UnifiedPaymentInvoicingSystem\DTO\UserDTO;
use PHPUnit\Framework\TestCase;

/**
 * Testovací třída pro UserDTO.
 * Ověřuje, že data předaná do DTO jsou správně nastavena a jednotlivé vlastnosti
 * objektu lze správně číst. Zaměřuje se na ověření správného přiřazení hodnot
 * pro osobní údaje uživatele, včetně informací o doručení a dalších uživatelských atributů.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
class UserDTOTest extends TestCase
{
    /**
     * Testuje správné přiřazení a získání vlastností objektu UserDTO.
     * Ověřuje, že hodnoty pro jméno, příjmení, email, telefonní číslo, doručovací informace,
     * pohlaví a typ uživatelského účtu jsou korektně nastaveny a odpovídají hodnotám předaným konstruktoru.
     */
    public function testPropertyAssignment(): void
    {
        $firstName = 'John';
        $lastName = 'Doe';
        $email = 'john.doe@example.com';
        $phoneNumber = '1234567890';
        $deliveryInformation = new DeliveryInformationDTO(
            country: 'Country',
            street: 'Street',
            streetNumber: '123',
            postalCode: '00000',
            city: 'City',
            countryCode: 'CC'
        );
        $gender = 'male';
        $userAccountType = 'admin';

        $userDto = new UserDTO(
            firstName: $firstName,
            lastName: $lastName,
            email: $email,
            phoneNumber: $phoneNumber,
            deliveryInformations: $deliveryInformation,
            gender: $gender,
            userAccountType: $userAccountType
        );

        $this->assertEquals($firstName, $userDto->firstName);
        $this->assertEquals($lastName, $userDto->lastName);
        $this->assertEquals($email, $userDto->email);
        $this->assertEquals($phoneNumber, $userDto->phoneNumber);
        $this->assertEquals($deliveryInformation, $userDto->deliveryInformations);
        $this->assertEquals($gender, $userDto->gender);
        $this->assertEquals($userAccountType, $userDto->userAccountType);
    }
}
