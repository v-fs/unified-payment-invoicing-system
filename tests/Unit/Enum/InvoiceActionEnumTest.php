<?php

declare(strict_types=1);

namespace Tests\Unit\Enum;

use Hejna\UnifiedPaymentInvoicingSystem\Enum\InvoiceActionEnum;
use PHPUnit\Framework\TestCase;

/**
 * Testovací třída pro InvoiceActionEnum.
 * Ověřuje správnost a funkčnost definovaných hodnot enumu InvoiceActionEnum a jejich použití.
 * Zaměřuje se na ověření správných řetězcových hodnot pro jednotlivé případy enumu a
 * také na přítomnost všech očekávaných případů enumu.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
class InvoiceActionEnumTest extends TestCase
{
    /**
     * Testuje, že hodnoty enumu InvoiceActionEnum jsou správně přiřazeny.
     * Ověřuje očekávané stringové hodnoty pro jednotlivé případy enumu.
     */
    public function testInvoiceActionEnumValues(): void
    {
        $this->assertEquals('mark_as_sent', InvoiceActionEnum::MARK_AS_SENT->value);
        $this->assertEquals('deliver', InvoiceActionEnum::DELIVER->value);
        $this->assertEquals('pay', InvoiceActionEnum::PAY->value);
        $this->assertEquals('pay_proforma', InvoiceActionEnum::PAY_PROFORMA->value);
        $this->assertEquals('pay_partial_proforma', InvoiceActionEnum::PAY_PARTIAL_PROFORMA->value);
        $this->assertEquals('remove_payment', InvoiceActionEnum::REMOVE_PAYMENT->value);
        $this->assertEquals('deliver_reminder', InvoiceActionEnum::DELIVER_REMINDER->value);
        $this->assertEquals('cancel', InvoiceActionEnum::CANCEL->value);
        $this->assertEquals('undo_cancel', InvoiceActionEnum::UNDO_CANCEL->value);
        $this->assertEquals('lock', InvoiceActionEnum::LOCK->value);
        $this->assertEquals('unlock', InvoiceActionEnum::UNLOCK->value);
        $this->assertEquals('mark_as_uncollectible', InvoiceActionEnum::MARK_AS_UNCOLLECTIBLE->value);
        $this->assertEquals('undo_uncollectible', InvoiceActionEnum::UNDO_UNCOLLECTIBLE->value);
    }

    /**
     * Testuje, že všechny případy enumu InvoiceActionEnum jsou správně definovány a přítomné.
     * Ověřuje, že pole vracené metodou cases() obsahuje všechny očekávané případy enumu
     * a že jejich počet odpovídá očekávanému.
     */
    public function testEnumCases(): void
    {
        $cases = InvoiceActionEnum::cases();
        $expectedCases = [
            'MARK_AS_SENT',
            'DELIVER',
            'PAY',
            'PAY_PROFORMA',
            'PAY_PARTIAL_PROFORMA',
            'REMOVE_PAYMENT',
            'DELIVER_REMINDER',
            'CANCEL',
            'UNDO_CANCEL',
            'LOCK',
            'UNLOCK',
            'MARK_AS_UNCOLLECTIBLE',
            'UNDO_UNCOLLECTIBLE',
        ];

        foreach ($cases as $case) {
            $this->assertContains(
                $case->name,
                $expectedCases,
                sprintf('Enum case %s is correctly defined.', $case->name)
            );
        }

        $this->assertCount(count($expectedCases), $cases, 'All expected enum cases are present.');
    }
}
