<?php

declare(strict_types=1);

namespace Tests\Unit\DTO;

use Hejna\UnifiedPaymentInvoicingSystem\DTO\ProductPaymentFormatDTO;
use PHPUnit\Framework\TestCase;

/**
 * Testovací třída pro ProductPaymentFormatDTO.
 * Ověřuje, že data předaná do DTO jsou správně nastavena a jednotlivé vlastnosti
 * objektu lze správně číst. Zaměřuje se na ověření správného přiřazení hodnot
 * vlastnostem name, amount a count.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
class ProductPaymentFormatDTOTest extends TestCase
{
    /**
     * Testuje správné přiřazení a získání vlastností objektu ProductPaymentFormatDTO.
     * Ověřuje, že hodnoty name, amount a count jsou korektně nastaveny a odpovídají
     * hodnotám předaným konstruktoru.
     */
    public function testPropertyAssignment(): void
    {
        $name = 'Product Name';
        $amount = 150.0;
        $count = 3;

        $dto = new ProductPaymentFormatDTO(name: $name, amount: $amount, count: $count);

        $this->assertEquals($name, $dto->name, 'The name property is not correctly assigned.');
        $this->assertEquals($amount, $dto->amount, 'The amount property is not correctly assigned.');
        $this->assertEquals($count, $dto->count, 'The count property is not correctly assigned.');
    }
}
