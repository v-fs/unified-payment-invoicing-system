<?php

declare(strict_types=1);

namespace Hejna\UnifiedPaymentInvoicingSystem\Enum;

/**
 * Výčtový typ Gender definuje možné hodnoty pro pohlaví.
 * Tento výčtový typ slouží k reprezentaci pohlaví v různých částech aplikace.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
enum Gender: string
{
    case MALE = 'male';
    case FEMALE = 'female';
}
