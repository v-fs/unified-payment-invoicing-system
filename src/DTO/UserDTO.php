<?php

declare(strict_types=1);

namespace Hejna\UnifiedPaymentInvoicingSystem\DTO;

/**
 * Třída UserDTO představuje DTO objekt obsahující informace o uživateli.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
class UserDTO
{
    /**
     * Konstruktor pro vytvoření nové instance objektu UserDTO.
     */
    public function __construct(
        public string $firstName,
        public string $lastName,
        public string $email,
        public string $phoneNumber,
        public DeliveryInformationDTO $deliveryInformations,
        public ?string $gender = null,
        public ?string $userAccountType = null,
    ) {
    }
}
