<?php

declare(strict_types=1);

namespace Hejna\UnifiedPaymentInvoicingSystem\DTO;

/**
 * Třída ProductPaymentFormatDTO představuje DTO objekt obsahující informace o produktech souvisejících s platebním formulářem.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
class ProductPaymentFormatDTO
{
    /**
     * Konstruktor pro vytvoření nové instance objektu ProductPaymentFormatDTO.
     */
    public function __construct(
        public string $name,
        public float $amount,
        public int $count,
    ) {
    }
}
