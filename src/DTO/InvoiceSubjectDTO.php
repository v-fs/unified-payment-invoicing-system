<?php

declare(strict_types=1);

namespace Hejna\UnifiedPaymentInvoicingSystem\DTO;

/**
 * Třída InvoiceSubjectDTO představuje DTO objekt obsahující informace o fakturačním subjektu.
 *
 * Autor: Daniel Hejna
 * Rok vytvoření: 2024
 * Fakulta ekonomických studií na Vysoké škole finanční a správní
 * Studijní obor: Aplikovaná informatika
 * Název BC práce: Integrace platebního systému do webových aplikací
 */
class InvoiceSubjectDTO
{
    public function __construct(
        public int $id,
        public ?string $customId,
        public string $type,
        public string $name,
        public ?string $street,
        public ?string $street2,
        public ?string $city,
        public ?string $zip,
        public ?string $country,
        public ?string $registrationNo,
        public ?string $vatNo,
        public ?string $localVatNo,
        public ?string $bankAccount,
        public ?string $iban,
        public ?string $variableSymbol,
        public ?bool $enabledReminders,
        public string $fullName,
        public string $email,
        public ?string $emailCopy,
        public ?string $phone,
        public ?string $privateNote,
        public ?string $url,
        public string $createdAt,
        public string $updatedAt
    ) {
    }
}
